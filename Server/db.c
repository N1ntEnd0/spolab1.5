#define _LARGEFILE64_SOURCE

#include "db.h"

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define SIGNATURE ("\xDE\xAD\xBA\xBE")

struct db * initStorage(int fd) {
    lseek64(fd, 0, SEEK_SET);

    write(fd, SIGNATURE, 4);

    uint64_t p = 0;
    write(fd, &p, sizeof(p));

    struct db * storage = malloc(sizeof(*storage));

    storage->fd = fd;
    storage->first_table = 0;
    return storage;
}

struct db * openStorage(int fd) {
    lseek64(fd, 0, SEEK_SET);

    char sign[4];
    if (read(fd, sign, 4) != 4) {
        errno = EINVAL;
        return NULL;
    }

    if (memcmp(sign, SIGNATURE, 4) != 0) {
        errno = EINVAL;
        return NULL;
    }

    struct db * storage = malloc(sizeof(*storage));
    storage->fd = fd;

    read(fd, &storage->first_table, sizeof(storage->first_table));
    return storage;
}

void deleteStorage(struct db * storage) {
    free(storage);
}

static char * readString(int fd) {
    uint16_t length;

    read(fd, &length, sizeof(length));

    char * str = malloc(sizeof(int8_t) * (length + 1));
    read(fd, str, length);
    str[length] = '\0';

    return str;
}

struct storage_table * findTable(struct db * storage, const char * name) {
    uint64_t pointer = storage->first_table;

    while (pointer) {
        lseek64(storage->fd, (off64_t) pointer, SEEK_SET);

        uint64_t next, first_row;
        read(storage->fd, &next, sizeof(next));
        read(storage->fd, &first_row, sizeof(first_row));

        char * table_name = readString(storage->fd);
        if (strcmp(table_name, name) != 0) {
            free(table_name);
            pointer = next;
            continue;
        }

        struct storage_table * table = malloc(sizeof(*table));
        table->storage = storage;
        table->position = pointer;
        table->next = next;
        table->first_row = first_row;
        table->name = table_name;

        read(storage->fd, &table->columns.amount, sizeof(table->columns.amount));
        table->columns.columns = malloc(sizeof(*table->columns.columns) * table->columns.amount);

        for (uint16_t i = 0; i < table->columns.amount; ++i) {
            table->columns.columns[i].name = readString(storage->fd);

            uint8_t type;
            read(storage->fd, &type, sizeof(type));
            table->columns.columns[i].type = (enum storage_column_type) type;
        }

        return table;
    }

    return NULL;
}

void tableDelete(struct storage_table * table) {
    if (table) {
        free(table->name);

        for (uint16_t i = 0; i < table->columns.amount; ++i) {
            free(table->columns.columns[i].name);
        }

        free(table->columns.columns);
    }

    free(table);
}

static uint64_t writeStorage(int fd, void * buf, size_t length) {
    uint64_t offset = lseek64(fd, 0, SEEK_END);

    write(fd, buf, length);
    return offset;
}

static uint64_t writeString(int fd, const char * str) {
    uint16_t length = strlen(str);

    uint64_t ret = writeStorage(fd, &length, sizeof(length));
    write(fd, str, length);
    return ret;
}

void addTable(struct storage_table * table) {
    struct storage_table * another_table = findTable(table->storage, table->name);

    if (another_table != NULL) {
        tableDelete(another_table);
        errno = EINVAL;
        return;
    }

    table->next = table->storage->first_table;
    table->position = writeStorage(table->storage->fd, &table->next, sizeof(table->next));
    table->storage->first_table = table->position;

    write(table->storage->fd, &table->first_row, sizeof(table->first_row));
    writeString(table->storage->fd, table->name);
    write(table->storage->fd, &table->columns.amount, sizeof(table->columns.amount));

    for (uint16_t i = 0; i < table->columns.amount; ++i) {
        writeString(table->storage->fd, table->columns.columns[i].name);

        uint8_t type = table->columns.columns[i].type;
        write(table->storage->fd, &type, sizeof(type));
    }

    lseek64(table->storage->fd, 4, SEEK_SET);
    write(table->storage->fd, &table->position, sizeof(table->position));
}

void removeTable(struct storage_table * table) {
    uint64_t pointer = table->storage->first_table;

    while (pointer) {
        lseek64(table->storage->fd, (off64_t) pointer, SEEK_SET);

        uint64_t next;
        read(table->storage->fd, &next, sizeof(next));
        if (next == table->position) {
            break;
        }

        pointer = next;
    }

    if (pointer == 0) {
        pointer = 4;
        table->storage->first_table = table->next;
    }

    lseek64(table->storage->fd, (off64_t) pointer, SEEK_SET);
    write(table->storage->fd, &table->next, sizeof(table->next));
}

struct storage_row * getFirstRow(struct storage_table * table) {
    if (table->first_row == 0) {
        return NULL;
    }

    struct storage_row * row = malloc(sizeof(*row));
    row->position = table->first_row;
    row->table = table;

    lseek64(table->storage->fd, (off64_t) row->position, SEEK_SET);
    read(table->storage->fd, &row->next, sizeof(row->next));

    return row;
}

struct storage_row * addRowToTable(struct storage_table * table) {
    struct storage_row * row = malloc(sizeof(*row));

    row->table = table;
    row->next = table->first_row;
    row->position = writeStorage(table->storage->fd, &row->next, sizeof(row->next));
    table->first_row = row->position;

    uint64_t null = 0;
    for (uint16_t i = 0; i < table->columns.amount; ++i) {
        write(table->storage->fd, &null, sizeof(null));
    }

    lseek64(table->storage->fd, (off64_t) (table->position + sizeof(uint64_t)), SEEK_SET);
    write(table->storage->fd, &table->first_row, sizeof(table->first_row));
    return row;
}

void deleteRow(struct storage_row * row) {
    free(row);
}

struct storage_row * rowNext(struct storage_row * row) {
    row->position = row->next;

    if (row->next == 0) {
        free(row);
        return NULL;
    }

    lseek64(row->table->storage->fd, (off64_t) row->position, SEEK_SET);
    read(row->table->storage->fd, &row->next, sizeof(row->next));
    return row;
}

void removeRow(struct storage_row * row) {
    uint64_t pointer = row->table->first_row;

    while (pointer) {
        lseek64(row->table->storage->fd, (off64_t) pointer, SEEK_SET);

        uint64_t next;
        read(row->table->storage->fd, &next, sizeof(next));

        if (next == row->position) {
            break;
        }

        pointer = next;
    }

    if (pointer == 0) {
        pointer = row->table->position + sizeof(uint64_t);
        row->table->first_row = row->next;
    }

    lseek64(row->table->storage->fd, (off64_t) pointer, SEEK_SET);
    write(row->table->storage->fd, &row->next, sizeof(row->next));
}

struct storage_value * getRowValue(struct storage_row * row, uint16_t index) {
    if (index >= row->table->columns.amount) {
        errno = EINVAL;
        return NULL;
    }

    lseek64(row->table->storage->fd, (off64_t) (row->position + (1 + index) * sizeof(uint64_t)), SEEK_SET);

    uint64_t pointer;
    read(row->table->storage->fd, &pointer, sizeof(pointer));

    if (pointer == 0) {
        return NULL;
    }

    lseek64(row->table->storage->fd, (off64_t) pointer, SEEK_SET);

    struct storage_value * value = malloc(sizeof(*value));
    value->type = row->table->columns.columns[index].type;

    switch (value->type) {
        case STORAGE_COLUMN_TYPE_INT:
            read(row->table->storage->fd, &value->value._int, sizeof(value->value._int));
            break;

        case STORAGE_COLUMN_TYPE_UINT:
            read(row->table->storage->fd, &value->value.uint, sizeof(value->value.uint));
            break;

        case STORAGE_COLUMN_TYPE_NUM:
            read(row->table->storage->fd, &value->value.num, sizeof(value->value.num));
            break;

        case STORAGE_COLUMN_TYPE_STR:
            value->value.str = readString(row->table->storage->fd);
            break;
    }

    return value;
}

void setRowValue(struct storage_row * row, uint16_t index, struct storage_value * value) {
    if (index >= row->table->columns.amount) {
        errno = EINVAL;
        return;
    }

    uint64_t pointer = 0;

    if (value) {
        if (row->table->columns.columns[index].type != value->type) {
            errno = EINVAL;
            return;
        }

        switch (value->type) {
            case STORAGE_COLUMN_TYPE_INT:
                pointer = writeStorage(row->table->storage->fd, &value->value._int, sizeof(value->value._int));
                break;

            case STORAGE_COLUMN_TYPE_UINT:
                pointer = writeStorage(row->table->storage->fd, &value->value.uint, sizeof(value->value.uint));
                break;

            case STORAGE_COLUMN_TYPE_NUM:
                pointer = writeStorage(row->table->storage->fd, &value->value.num, sizeof(value->value.num));
                break;

            case STORAGE_COLUMN_TYPE_STR:
                pointer = writeString(row->table->storage->fd, value->value.str);
                break;
        }
    }

    lseek64(row->table->storage->fd, (off64_t) (row->position + (1 + index) * sizeof(uint64_t)), SEEK_SET);
    write(row->table->storage->fd, &pointer, sizeof(pointer));
}

void destroyValue(struct storage_value value) {
    switch (value.type) {
        case STORAGE_COLUMN_TYPE_STR:
            free(value.value.str);

        default:
            break;
    }
}

const char * convertTypeToString(enum storage_column_type type) {
    switch (type) {
        case STORAGE_COLUMN_TYPE_INT:
            return "int";

        case STORAGE_COLUMN_TYPE_UINT:
            return "uint";

        case STORAGE_COLUMN_TYPE_NUM:
            return "num";

        case STORAGE_COLUMN_TYPE_STR:
            return "str";

        default:
            return NULL;
    }
}

struct storage_joined_table * joinedTableNew(unsigned int amount) {
    struct storage_joined_table * table = malloc(sizeof(*table));

    table->tables.amount = amount;
    table->tables.tables = calloc(amount, sizeof(*table->tables.tables));

    return table;
}

struct storage_joined_table * joinedTableWrap(struct storage_table * table) {
    if (!table) {
        return NULL;
    }

    struct storage_joined_table * joined_table = joinedTableNew(1);
    joined_table->tables.tables[0].table = table;
    joined_table->tables.tables[0].t_column_index = 0;
    joined_table->tables.tables[0].s_column_index = 0;

    return joined_table;
}

void joinedTableDelete(struct storage_joined_table * table) {
    if (table) {
        for (int i = 0; i < table->tables.amount; ++i) {
            tableDelete(table->tables.tables[i].table);
        }

        free(table->tables.tables);
    }

    free(table);
}

uint16_t joinedTableGetColumnsAmount(struct storage_joined_table * table) {
    uint16_t amount = 0;

    for (int i = 0; i < table->tables.amount; ++i) {
        amount += table->tables.tables[i].table->columns.amount;
    }

    return amount;
}

struct storage_column joinedTableGetColumn(struct storage_joined_table * table, uint16_t index) {
    for (int i = 0; i < table->tables.amount; ++i) {
        if (index < table->tables.tables[i].table->columns.amount) {
            return table->tables.tables[i].table->columns.columns[index];
        }

        index -= table->tables.tables[i].table->columns.amount;
    }

    abort();
}

static bool valueIsEquals(struct storage_value * a, struct storage_value * b) {
    if (a == NULL || b == NULL) {
        return a == b;
    }

    switch (a->type) {
        case STORAGE_COLUMN_TYPE_INT:
            switch (b->type) {
                case STORAGE_COLUMN_TYPE_INT:
                    return a->value._int == b->value._int;

                case STORAGE_COLUMN_TYPE_UINT:
                    if (a->value._int < 0) {
                        return false;
                    }

                    return ((uint64_t) a->value._int) == b->value.uint;

                case STORAGE_COLUMN_TYPE_NUM:
                    return ((double) a->value._int) == b->value.num;

                case STORAGE_COLUMN_TYPE_STR:
                    return false;
            }

        case STORAGE_COLUMN_TYPE_UINT:
            switch (b->type) {
                case STORAGE_COLUMN_TYPE_INT:
                    if (b->value._int < 0) {
                        return false;
                    }

                    return a->value.uint == ((uint64_t) b->value._int);

                case STORAGE_COLUMN_TYPE_UINT:
                    return a->value.uint == b->value.uint;

                case STORAGE_COLUMN_TYPE_NUM:
                    return ((double) a->value.uint) == b->value.num;

                case STORAGE_COLUMN_TYPE_STR:
                    return false;
            }

        case STORAGE_COLUMN_TYPE_NUM:
            switch (b->type) {
                case STORAGE_COLUMN_TYPE_INT:
                    return a->value.num == ((double) b->value._int);

                case STORAGE_COLUMN_TYPE_UINT:
                    return a->value.num == ((double) b->value.uint);

                case STORAGE_COLUMN_TYPE_NUM:
                    return a->value.num == b->value.num;

                case STORAGE_COLUMN_TYPE_STR:
                    return false;
            }

        case STORAGE_COLUMN_TYPE_STR:
            switch (b->type) {
                case STORAGE_COLUMN_TYPE_INT:
                case STORAGE_COLUMN_TYPE_UINT:
                case STORAGE_COLUMN_TYPE_NUM:
                    return false;

                case STORAGE_COLUMN_TYPE_STR:
                    return strcmp(a->value.str, b->value.str) == 0;
            }
    }
}

static bool joinedRowIsOn(struct joinedRow * row, uint16_t index) {
    return valueIsEquals(
            joinedRowGetValue(row, row->table->tables.tables[index].s_column_index),
            getRowValue(row->rows[index], row->table->tables.tables[index].t_column_index)
    );
}

static void joinedRowRoll(struct joinedRow * row) {
    for (int i = 1; i < row->table->tables.amount; ++i) {
        if (!joinedRowIsOn(row, i)) {
            row->rows[i] = rowNext(row->rows[i]);

            for (int j = i + 1; j < row->table->tables.amount; ++j) {
                deleteRow(row->rows[j]);
                row->rows[j] = getFirstRow(row->table->tables.tables[j].table);
            }

            for (int j = i; j > 0; --j) {
                if (row->rows[j] == NULL) {
                    row->rows[j] = getFirstRow(row->table->tables.tables[j].table);
                    row->rows[j - 1] = rowNext(row->rows[j - 1]);
                }
            }

            if (row->rows[0] == NULL) {
                return;
            }

            i = 0;
        }
    }
}

struct joinedRow * joinedTableGetFirstRow(struct storage_joined_table * table) {
    struct joinedRow * row = malloc(sizeof(*row));

    row->table = table;
    row->rows = malloc(sizeof(struct storage_row *) * table->tables.amount);
    for (int i = 0; i < table->tables.amount; ++i) {
        row->rows[i] = NULL;
    }

    for (int i = 0; i < table->tables.amount; ++i) {
        row->rows[i] = getFirstRow(table->tables.tables[i].table);

        if (row->rows[i] == NULL) {
            joinedRowDelete(row);
            return NULL;
        }
    }

    joinedRowRoll(row);
    if (row->rows[0] == NULL) {
        joinedRowDelete(row);
        return NULL;
    }

    return row;
}

void joinedRowDelete(struct joinedRow * row) {
    if (row) {
        for (int i = 0; i < row->table->tables.amount; ++i) {
            deleteRow(row->rows[i]);
        }

        free(row->rows);
    }

    free(row);
}

struct joinedRow * joinedRowNext(struct joinedRow * row) {
    uint16_t last_index = row->table->tables.amount - 1;

    row->rows[last_index] = rowNext(row->rows[last_index]);
    for (int i = (int) last_index; i > 0; --i) {
        if (row->rows[i] == NULL) {
            row->rows[i] = getFirstRow(row->table->tables.tables[i].table);
            row->rows[i - 1] = rowNext(row->rows[i - 1]);
        }
    }

    if (row->rows[0] == NULL) {
        joinedRowDelete(row);
        return NULL;
    }

    joinedRowRoll(row);
    if (row->rows[0] == NULL) {
        joinedRowDelete(row);
        return NULL;
    }

    return row;
}

struct storage_value * joinedRowGetValue(struct joinedRow * row, uint16_t index) {
    for (int i = 0; i < row->table->tables.amount; ++i) {
        if (index < row->table->tables.tables[i].table->columns.amount) {
            return getRowValue(row->rows[i], index);
        }

        index -= row->table->tables.tables[i].table->columns.amount;
    }

    return NULL;
}
