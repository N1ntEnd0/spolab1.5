#pragma once

#include <json-c/json.h>

#include "../Server/db.h"

enum action {
    JSON_API_TYPE_CREATE_TABLE = 0,
    JSON_API_TYPE_DROP_TABLE = 1,
    JSON_API_TYPE_INSERT = 2,
    JSON_API_TYPE_DELETE = 3,
    JSON_API_TYPE_SELECT = 4,
    JSON_API_TYPE_UPDATE = 5,
};

struct requestCreateTable {
    char * table_name;
    struct {
        unsigned int amount;
        struct {
            char * name;
            enum storage_column_type type;
        } * columns;
    } columns;
};

struct requestDropTable {
    char * table_name;
};

struct requestInsert {
    char * table_name;
    struct {
        unsigned int amount;
        char ** columns;
    } columns;
    struct {
        unsigned int amount;
        struct storage_value ** values;
    } values;
};

enum operator {
    JSON_API_OPERATOR_EQ = 0,
    JSON_API_OPERATOR_NE = 1,
    JSON_API_OPERATOR_LT = 2,
    JSON_API_OPERATOR_GT = 3,
    JSON_API_OPERATOR_LE = 4,
    JSON_API_OPERATOR_GE = 5,
    JSON_API_OPERATOR_AND = 6,
    JSON_API_OPERATOR_OR = 7,
};

struct where {
    enum operator op;

    union {
        struct {
            char * column;
            struct storage_value * value;
        };

        struct {
            struct where * left;
            struct where * right;
        };
    };
};

struct requestDelete {
    char * table_name;
    struct where * where;
};

struct requestSelect {
    char * table_name;
    struct {
        unsigned int amount;
        char ** columns;
    } columns;
    struct where * where;
    unsigned int offset;
    unsigned int limit;
    struct {
        unsigned int amount;
        struct {
            char * table;
            char * t_column;
            char * s_column;
        } * joins;
    } joins;
};

struct requestUpdate {
    char * table_name;
    struct {
        unsigned int amount;
        char ** columns;
    } columns;
    struct {
        unsigned int amount;
        struct storage_value ** values;
    } values;
    struct where * where;
};

enum action getAction(struct json_object * object);

struct requestDelete deleteRequest(struct json_object * object);
struct requestSelect selectRequest(struct json_object * object);
struct requestUpdate updateRequest(struct json_object * object);
struct requestCreateTable createTableRequest(struct json_object * object);
struct requestDropTable dropTableRequest(struct json_object * object);
struct requestInsert insertRequest(struct json_object * object);

struct json_object * apiError(const char * msg);
struct json_object * apiSuccess(struct json_object * answer);

struct json_object * jsonApiFromValue(struct storage_value * value);
