#pragma once

#include <stdint.h>

static const char * const JOINED_TABLE_NAME = "joined table";

enum storage_column_type {
    STORAGE_COLUMN_TYPE_INT = 0,
    STORAGE_COLUMN_TYPE_UINT = 1,
    STORAGE_COLUMN_TYPE_NUM = 2,
    STORAGE_COLUMN_TYPE_STR = 3,
};

struct db {
    int fd;
    uint64_t first_table;
};

struct storage_column {
    char * name;
    enum storage_column_type type;
};

struct storage_table {
    struct db * storage;

    uint64_t position;
    uint64_t next;

    uint64_t first_row;
    char * name;

    struct {
        uint16_t amount;
        struct storage_column * columns;
    } columns;
};

struct storage_row {
    struct storage_table * table;

    uint64_t position;
    uint64_t next;
};

struct storage_value {
    enum storage_column_type type;

    union {
        int64_t _int;
        uint64_t uint;
        double num;
        char * str;
    } value;
};

struct storage_joined_table {
    struct {
        unsigned int amount;
        struct {
            struct storage_table * table;
            uint16_t t_column_index;
            uint16_t s_column_index;
        } * tables;
    } tables;
};

struct joinedRow {
    struct storage_joined_table * table;
    struct storage_row ** rows;
};


struct db * initStorage(int fd);
struct db * openStorage(int fd);
void deleteStorage(struct db * storage);

struct storage_table * findTable(struct db * storage, const char * name);


void tableDelete(struct storage_table * table);

void addTable(struct storage_table * table);
void removeTable(struct storage_table * table);
struct storage_row * getFirstRow(struct storage_table * table);
struct storage_row * addRowToTable(struct storage_table * table);


void deleteRow(struct storage_row * row);

struct storage_row * rowNext(struct storage_row * row);
void removeRow(struct storage_row * row);
struct storage_value * getRowValue(struct storage_row * row, uint16_t index);
void setRowValue(struct storage_row * row, uint16_t index, struct storage_value * value);

//void destroyValue(struct storage_value value);
//void storage_value_delete(struct storage_value * value);

const char * convertTypeToString(enum storage_column_type type);

struct storage_joined_table * joinedTableNew(unsigned int amount);
struct storage_joined_table * joinedTableWrap(struct storage_table * table);
void joinedTableDelete(struct storage_joined_table * table);

uint16_t joinedTableGetColumnsAmount(struct storage_joined_table * table);
struct storage_column joinedTableGetColumn(struct storage_joined_table * table, uint16_t index);
struct joinedRow * joinedTableGetFirstRow(struct storage_joined_table * table);


void joinedRowDelete(struct joinedRow * row);

struct joinedRow * joinedRowNext(struct joinedRow * row);
struct storage_value * joinedRowGetValue(struct joinedRow * row, uint16_t index);
