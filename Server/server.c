#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <signal.h>
#include "db.h"
#include "../JSON/jsonApi.h"

static volatile bool closing = false;

static void closeHandler(int sig, siginfo_t * info, void * context) {
    closing = true;
}

static struct json_object * requestCreateTable(struct requestCreateTable request, struct db * storage) {
    struct storage_table * table = malloc(sizeof(*table));

    table->storage = storage;
    table->position = 0;
    table->next = 0;
    table->first_row = 0;
    table->name = strdup(request.table_name);
    table->columns.amount = request.columns.amount;
    table->columns.columns = malloc(sizeof(*table->columns.columns) * request.columns.amount);

    for (int i = 0; i < request.columns.amount; ++i) {
        table->columns.columns[i].name = strdup(request.columns.columns[i].name);
        table->columns.columns[i].type = request.columns.columns[i].type;
    }

    errno = 0;
    addTable(table);
    bool error = errno != 0;

    tableDelete(table);

    if (error) {
        return apiError("a table with the same name is already exists");
    } else {
        return apiSuccess(json_object_new_object());
    }
}

static struct json_object * requestDropTable(struct requestDropTable request, struct db * storage) {
    struct storage_table * table = findTable(storage, request.table_name);

    if (!table) {
        return apiError("table with the specified name is not exists");
    }

    removeTable(table);
    tableDelete(table);
    return apiSuccess(json_object_new_object());
}

static struct json_object * mapColumnsToIndexes(unsigned int request_columns_amount, char ** request_columns_names,
                                                struct storage_joined_table * table, unsigned int * columns_amount, unsigned int ** columns_indexes) {
    unsigned int columns_count = request_columns_amount;

    uint16_t table_columns_amount = joinedTableGetColumnsAmount(table);

    if (columns_count == 0) {
        columns_count = table_columns_amount;
    }

    *columns_indexes = malloc(sizeof(**columns_indexes) * columns_count);
    if (request_columns_amount == 0) {
        for (unsigned int i = 0; i < columns_count; ++i) {
            (*columns_indexes)[i] = i;
        }
    } else {
        for (unsigned int i = 0; i < columns_count; ++i) {
            bool found = false;

            for (unsigned int j = 0; j < table_columns_amount; ++j) {
                if (strcmp(request_columns_names[i], joinedTableGetColumn(table, j).name) == 0) {
                    (*columns_indexes)[i] = j;
                    found = true;
                    break;
                }
            }

            if (!found) {
                size_t msg_length = 41 + strlen(request_columns_names[i]);

                char msg[msg_length];
                snprintf(msg, msg_length, "column with name %s is not exists in table", request_columns_names[i]);

                joinedTableDelete(table);
                return apiError(msg);
            }
        }
    }

    *columns_amount = columns_count;
    return NULL;
}

static struct json_object * checkValues(unsigned int request_values_amount, struct storage_value ** request_values_values,
                                        struct storage_table * table, unsigned int columns_amount, const unsigned int * columns_indexes) {

    if (request_values_amount != columns_amount) {
        return apiError("values amount is not equals to columns amount");
    }

    for (unsigned int i = 0; i < columns_amount; ++i) {
        if (request_values_values[i] == NULL) {
            continue;
        }

        struct storage_column column = table->columns.columns[columns_indexes[i]];
        if (request_values_values[i]->type == column.type) {
            continue;
        }

        switch (request_values_values[i]->type) {
            case STORAGE_COLUMN_TYPE_INT:
                if (column.type == STORAGE_COLUMN_TYPE_UINT) {
                    if (request_values_values[i]->value._int >= 0) {
                        request_values_values[i]->type = STORAGE_COLUMN_TYPE_UINT;
                        request_values_values[i]->value.uint = (uint64_t) request_values_values[i]->value._int;
                        continue;
                    }
                }
                break;

            case STORAGE_COLUMN_TYPE_UINT:
                if (column.type == STORAGE_COLUMN_TYPE_INT) {
                    if (request_values_values[i]->value.uint <= INT64_MAX) {
                        request_values_values[i]->type = STORAGE_COLUMN_TYPE_INT;
                        request_values_values[i]->value._int = (int64_t) request_values_values[i]->value.uint;
                        continue;
                    }
                }
                break;

            default:
                break;
        }

        const char * col_type = convertTypeToString(column.type);
        const char * val_type = convertTypeToString(request_values_values[i]->type);
        size_t msg_length = 47 + strlen(column.name) + strlen(col_type) + strlen(val_type);

        char msg[msg_length];
        snprintf(msg, msg_length, "value for column with name %s (%s) has wrong type %s",
                 column.name, col_type, val_type);
        return apiError(msg);
    }

    return NULL;
}

static struct json_object * requestInsert(struct requestInsert request, struct db * storage) {
    struct storage_table * table = findTable(storage, request.table_name);

    if (!table) {
        return apiError("table with the specified name is not exists");
    }

    unsigned int columns_amount;
    unsigned int * columns_indexes;
    struct storage_joined_table * joined_table = joinedTableWrap(table);

    {
        struct json_object * error = mapColumnsToIndexes(request.columns.amount, request.columns.columns,
                                                         joined_table, &columns_amount, &columns_indexes);

        if (error) {
            joinedTableDelete(joined_table);
            return error;
        }
    }

    {
        struct json_object * error = checkValues(request.values.amount, request.values.values, table, columns_amount,
                                                 columns_indexes);

        if (error) {
            free(columns_indexes);
            joinedTableDelete(joined_table);
            return error;
        }
    }

    struct storage_row * row = addRowToTable(table);
    for (unsigned int i = 0; i < columns_amount; ++i) {
        setRowValue(row, columns_indexes[i], request.values.values[i]);
    }

    free(columns_indexes);
    deleteRow(row);
    joinedTableDelete(joined_table);
    return apiSuccess(json_object_new_object());
}

static struct json_object * whereCorrect(struct storage_joined_table * table, struct where * where) {
    uint16_t table_columns_amount = joinedTableGetColumnsAmount(table);

    switch (where->op) {
        case JSON_API_OPERATOR_EQ:
        case JSON_API_OPERATOR_NE:
            if (where->value == NULL) {
                return NULL;
            }

        case JSON_API_OPERATOR_LT:
        case JSON_API_OPERATOR_GT:
        case JSON_API_OPERATOR_LE:
        case JSON_API_OPERATOR_GE:
            if (where->value == NULL) {
                return apiError("NULL value is not comparable");
            }
            for (unsigned int i = 0; i < table_columns_amount; ++i) {
                struct storage_column column = joinedTableGetColumn(table, i);

                if (strcmp(column.name, where->column) == 0) {
                    switch (column.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                        case STORAGE_COLUMN_TYPE_UINT:
                        case STORAGE_COLUMN_TYPE_NUM:
                            switch (where->value->type) {
                                case STORAGE_COLUMN_TYPE_INT:
                                case STORAGE_COLUMN_TYPE_UINT:
                                case STORAGE_COLUMN_TYPE_NUM:
                                    return NULL;
                                case STORAGE_COLUMN_TYPE_STR:
                                    break;
                            }
                            break;

                        case STORAGE_COLUMN_TYPE_STR:
                            if (where->value->type == STORAGE_COLUMN_TYPE_STR) {
                                return NULL;
                            }
                            break;
                    }

                    const char * column_type = convertTypeToString(column.type);
                    const char * value_type = convertTypeToString(where->value->type);
                    size_t msg_length = 31 + strlen(column_type) + strlen(value_type);
                    char msg[msg_length];

                    snprintf(msg, msg_length, "types %s and %s are not comparable", column_type, value_type);
                    return apiError(msg);
                }
            }

            {
                size_t msg_length = 41 + strlen(where->column);
                char msg[msg_length];
                snprintf(msg, msg_length, "column with name %s is not exists in table", where->column);

                return apiError(msg);
            }

        case JSON_API_OPERATOR_AND:
        case JSON_API_OPERATOR_OR:
        {
            struct json_object * left = whereCorrect(table, where->left);
            if (left != NULL) {
                return left;
            }
            return whereCorrect(table, where->right);
        }
    }
}

static bool compareValuesNotNull(enum operator op, struct storage_value left, struct storage_value right) {
    switch (op) {
        case JSON_API_OPERATOR_EQ:
            switch (left.type) {
                case STORAGE_COLUMN_TYPE_INT:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                            return left.value._int == right.value._int;

                        case STORAGE_COLUMN_TYPE_UINT:
                            if (left.value._int < 0) {
                                return false;
                            }

                            return ((uint64_t) left.value._int) == right.value.uint;
                        case STORAGE_COLUMN_TYPE_NUM:
                            return ((double) left.value._int) == right.value.num;
                        case STORAGE_COLUMN_TYPE_STR:
                            return false;
                    }

                case STORAGE_COLUMN_TYPE_UINT:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                            if (right.value._int < 0) {
                                return false;
                            }

                            return left.value.uint == ((uint64_t) right.value._int);
                        case STORAGE_COLUMN_TYPE_UINT:
                            return left.value.uint == right.value.uint;
                        case STORAGE_COLUMN_TYPE_NUM:
                            return ((double) left.value.uint) == right.value.num;
                        case STORAGE_COLUMN_TYPE_STR:
                            return false;
                    }

                case STORAGE_COLUMN_TYPE_NUM:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                            return left.value.num == ((double) right.value._int);
                        case STORAGE_COLUMN_TYPE_UINT:
                            return left.value.num == ((double) right.value.uint);
                        case STORAGE_COLUMN_TYPE_NUM:
                            return left.value.num == right.value.num;
                        case STORAGE_COLUMN_TYPE_STR:
                            return false;
                    }
                case STORAGE_COLUMN_TYPE_STR:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                        case STORAGE_COLUMN_TYPE_UINT:
                        case STORAGE_COLUMN_TYPE_NUM:
                            return false;

                        case STORAGE_COLUMN_TYPE_STR:
                            return strcmp(left.value.str, right.value.str) == 0;
                    }
            }

        case JSON_API_OPERATOR_NE:
            return !compareValuesNotNull(JSON_API_OPERATOR_EQ, left, right);

        case JSON_API_OPERATOR_LT:
            switch (left.type) {
                case STORAGE_COLUMN_TYPE_INT:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                            return left.value._int < right.value._int;

                        case STORAGE_COLUMN_TYPE_UINT:
                            if (left.value._int < 0) {
                                return true;
                            }

                            return ((uint64_t) left.value._int) < right.value.uint;

                        case STORAGE_COLUMN_TYPE_NUM:
                            return ((double) left.value._int) < right.value.num;

                        case STORAGE_COLUMN_TYPE_STR:
                            return false;
                    }

                case STORAGE_COLUMN_TYPE_UINT:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                            if (right.value._int < 0) {
                                return false;
                            }

                            return left.value.uint < ((uint64_t) right.value._int);

                        case STORAGE_COLUMN_TYPE_UINT:
                            return left.value.uint < right.value.uint;

                        case STORAGE_COLUMN_TYPE_NUM:
                            return ((double) left.value.uint) < right.value.num;

                        case STORAGE_COLUMN_TYPE_STR:
                            return false;
                    }

                case STORAGE_COLUMN_TYPE_NUM:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                            return left.value.num < ((double) right.value._int);

                        case STORAGE_COLUMN_TYPE_UINT:
                            return left.value.num < ((double) right.value.uint);

                        case STORAGE_COLUMN_TYPE_NUM:
                            return left.value.num < right.value.num;

                        case STORAGE_COLUMN_TYPE_STR:
                            return false;
                    }

                case STORAGE_COLUMN_TYPE_STR:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                        case STORAGE_COLUMN_TYPE_UINT:
                        case STORAGE_COLUMN_TYPE_NUM:
                            return false;

                        case STORAGE_COLUMN_TYPE_STR:
                            return strcmp(left.value.str, right.value.str) < 0;
                    }
            }

        case JSON_API_OPERATOR_GT:
            switch (left.type) {
                case STORAGE_COLUMN_TYPE_INT:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                            return left.value._int > right.value._int;
                        case STORAGE_COLUMN_TYPE_UINT:
                            if (left.value._int < 0) {
                                return false;
                            }
                            return ((uint64_t) left.value._int) > right.value.uint;
                        case STORAGE_COLUMN_TYPE_NUM:
                            return ((double) left.value._int) > right.value.num;
                        case STORAGE_COLUMN_TYPE_STR:
                            return false;
                    }

                case STORAGE_COLUMN_TYPE_UINT:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                            if (right.value._int < 0) {
                                return true;
                            }

                            return left.value.uint > ((uint64_t) right.value._int);

                        case STORAGE_COLUMN_TYPE_UINT:
                            return left.value.uint > right.value.uint;
                        case STORAGE_COLUMN_TYPE_NUM:
                            return ((double) left.value.uint) > right.value.num;
                        case STORAGE_COLUMN_TYPE_STR:
                            return false;
                    }

                case STORAGE_COLUMN_TYPE_NUM:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                            return left.value.num > ((double) right.value._int);
                        case STORAGE_COLUMN_TYPE_UINT:
                            return left.value.num > ((double) right.value.uint);
                        case STORAGE_COLUMN_TYPE_NUM:
                            return left.value.num > right.value.num;
                        case STORAGE_COLUMN_TYPE_STR:
                            return false;
                    }

                case STORAGE_COLUMN_TYPE_STR:
                    switch (right.type) {
                        case STORAGE_COLUMN_TYPE_INT:
                        case STORAGE_COLUMN_TYPE_UINT:
                        case STORAGE_COLUMN_TYPE_NUM:
                            return false;

                        case STORAGE_COLUMN_TYPE_STR:
                            return strcmp(left.value.str, right.value.str) > 0;
                    }
            }

        case JSON_API_OPERATOR_LE:
            return !compareValuesNotNull(JSON_API_OPERATOR_GT, left, right);

        case JSON_API_OPERATOR_GE:
            return !compareValuesNotNull(JSON_API_OPERATOR_LT, left, right);

        default:
            return false;
    }
}

static bool compareValues(enum operator op, struct storage_value * left, struct storage_value * right) {
    switch (op) {
        case JSON_API_OPERATOR_EQ:
            if (left == NULL || right == NULL) {
                return left == NULL && right == NULL;
            }
            break;

        case JSON_API_OPERATOR_NE:
            if (left == NULL || right == NULL) {
                return (left == NULL) != (right == NULL);
            }
            break;

        case JSON_API_OPERATOR_LT:
        case JSON_API_OPERATOR_GT:
        case JSON_API_OPERATOR_LE:
        case JSON_API_OPERATOR_GE:
            if (left == NULL || right == NULL) {
                return false;
            }
            break;

        default:
            return false;
    }
    return compareValuesNotNull(op, *left, *right);
}

static bool evalWhere(struct joinedRow * row, struct where * where) {
    uint16_t table_columns_amount = joinedTableGetColumnsAmount(row->table);

    switch (where->op) {
        case JSON_API_OPERATOR_EQ:
        case JSON_API_OPERATOR_NE:
        case JSON_API_OPERATOR_LT:
        case JSON_API_OPERATOR_GT:
        case JSON_API_OPERATOR_LE:
        case JSON_API_OPERATOR_GE:
            for (unsigned int i = 0; i < table_columns_amount; ++i) {
                if (strcmp(joinedTableGetColumn(row->table, i).name, where->column) == 0) {
                    return compareValues(where->op, joinedRowGetValue(row, i), where->value);
                }
            }

            errno = EINVAL;
            return false;
        case JSON_API_OPERATOR_AND:
            return evalWhere(row, where->left) && evalWhere(row, where->right);
        case JSON_API_OPERATOR_OR:
            return evalWhere(row, where->left) || evalWhere(row, where->right);
    }
}

static struct json_object * requestDelete(struct requestDelete request, struct db * storage) {
    struct storage_table * table = findTable(storage, request.table_name);

    if (!table) {
        return apiError("table with the specified name is not exists");
    }

    struct storage_joined_table * joined_table = joinedTableWrap(table);

    if (request.where) {
        struct json_object * error = whereCorrect(joined_table, request.where);

        if (error) {
            joinedTableDelete(joined_table);
            return error;
        }
    }
    unsigned long long amount = 0;
    for (struct joinedRow * row = joinedTableGetFirstRow(joined_table); row; row = joinedRowNext(row)) {
        if (request.where == NULL || evalWhere(row, request.where)) {
            removeRow(row->rows[0]);
            ++amount;
        }
    }
    joinedTableDelete(joined_table);
    struct json_object * answer = json_object_new_object();
    json_object_object_add(answer, "amount", json_object_new_uint64(amount));
    return apiSuccess(answer);
}

static struct json_object * requestSelect(struct requestSelect request, struct db * storage) {
    if (request.limit > 1000) {
        return apiError("limit is too high");
    }
    struct storage_table * table = findTable(storage, request.table_name);
    if (!table) {
        return apiError("table with the specified name is not exists");
    }

    struct storage_joined_table * joined_table = joinedTableNew(request.joins.amount + 1);
    joined_table->tables.tables[0].table = table;
    joined_table->tables.tables[0].t_column_index = 0;
    joined_table->tables.tables[0].s_column_index = 0;

    for (int i = 0; i < request.joins.amount; ++i) {
        joined_table->tables.tables[i + 1].table = findTable(storage, request.joins.joins[i].table);
        if (!joined_table->tables.tables[i + 1].table) {
            joinedTableDelete(joined_table);
            return apiError("table with the specified name is not exists");
        }
        joined_table->tables.tables[i + 1].t_column_index = (uint16_t) -1;
        for (int j = 0; j < joined_table->tables.tables[i + 1].table->columns.amount; ++j) {
            if (strcmp(request.joins.joins[i].t_column, joined_table->tables.tables[i + 1].table->columns.columns[j].name) == 0) {
                joined_table->tables.tables[i + 1].t_column_index = j;
                break;
            }
        }
        if (joined_table->tables.tables[i + 1].t_column_index >= joined_table->tables.tables[i + 1].table->columns.amount) {
            joinedTableDelete(joined_table);
            return apiError("column with the specified name is not exists in table");
        }

        uint16_t slice_columns = 0;
        joined_table->tables.tables[i + 1].s_column_index = (uint16_t) -1;
        for (int tbl_index = 0, col_index = 0; tbl_index <= i; ++tbl_index) {
            for (int tbl_col_index = 0; tbl_col_index < joined_table->tables.tables[tbl_index].table->columns.amount; ++tbl_col_index, ++col_index) {
                if (strcmp(request.joins.joins[i].s_column, joined_table->tables.tables[tbl_index].table->columns.columns[tbl_col_index].name) == 0) {
                    joined_table->tables.tables[i + 1].s_column_index = col_index;
                    break;
                }
            }

            slice_columns += joined_table->tables.tables[tbl_index].table->columns.amount;
            if (joined_table->tables.tables[i + 1].s_column_index < slice_columns) {
                break;
            }
        }

        if (joined_table->tables.tables[i + 1].s_column_index >= slice_columns) {
            joinedTableDelete(joined_table);
            return apiError("column with the specified name is not exists in the join slice");
        }
    }

    if (request.where) {
        struct json_object * error = whereCorrect(joined_table, request.where);

        if (error) {
            joinedTableDelete(joined_table);
            return error;
        }
    }

    unsigned int columns_amount;
    unsigned int * columns_indexes;

    {
        struct json_object * error = mapColumnsToIndexes(request.columns.amount, request.columns.columns,
                                                         joined_table, &columns_amount, &columns_indexes);

        if (error) {
            joinedTableDelete(joined_table);
            return error;
        }
    }

    struct json_object * answer = json_object_new_object();
    {
        struct json_object * columns = json_object_new_array_ext((int) columns_amount);

        for (unsigned int i = 0; i < columns_amount; ++i) {
            json_object_array_add(columns, json_object_new_string(
                    joinedTableGetColumn(joined_table, columns_indexes[i]).name));
        }

        json_object_object_add(answer, "columns", columns);
    }

    {
        struct json_object * values = json_object_new_array_ext((int) request.limit);

        unsigned int offset = 0, amount = 0;
        for (struct joinedRow * row = joinedTableGetFirstRow(joined_table); row; row = joinedRowNext(row)) {
            if (request.where == NULL || evalWhere(row, request.where)) {
                if (offset < request.offset) {
                    ++offset;
                    continue;
                }

                if (amount == request.limit) {
                    break;
                }

                struct json_object * values_row = json_object_new_array_ext((int) columns_amount);

                for (unsigned int i = 0; i < columns_amount; ++i) {
                    json_object_array_add(values_row, jsonApiFromValue(joinedRowGetValue(row, columns_indexes[i])));
                }

                json_object_array_add(values, values_row);
                ++amount;
            }
        }

        json_object_object_add(answer, "values", values);
    }

    free(columns_indexes);
    joinedTableDelete(joined_table);
    return apiSuccess(answer);
}

static struct json_object * requestUpdate(struct requestUpdate request, struct db * storage) {
    struct storage_table * table = findTable(storage, request.table_name);

    if (!table) {
        return apiError("table with the specified name is not exists");
    }

    struct storage_joined_table * joined_table = joinedTableWrap(table);

    if (request.where) {
        struct json_object * error = whereCorrect(joined_table, request.where);

        if (error) {
            joinedTableDelete(joined_table);
            return error;
        }
    }

    unsigned int columns_amount;
    unsigned int * columns_indexes;

    {
        struct json_object * error = mapColumnsToIndexes(request.columns.amount, request.columns.columns,
                                                         joined_table, &columns_amount, &columns_indexes);
        if (error) {
            joinedTableDelete(joined_table);
            return error;
        }
    }

    {
        struct json_object * error = checkValues(request.values.amount, request.values.values, table, columns_amount,
                                                 columns_indexes);
        if (error) {
            free(columns_indexes);
            joinedTableDelete(joined_table);
            return error;
        }
    }

    unsigned long long amount = 0;
    for (struct joinedRow * row = joinedTableGetFirstRow(joined_table); row; row = joinedRowNext(row)) {
        if (request.where == NULL || evalWhere(row, request.where)) {
            for (unsigned int i = 0; i < columns_amount; ++i) {
                setRowValue(row->rows[0], columns_indexes[i], request.values.values[i]);
            }

            ++amount;
        }
    }

    free(columns_indexes);
    joinedTableDelete(joined_table);
    struct json_object * answer = json_object_new_object();
    json_object_object_add(answer, "amount", json_object_new_uint64(amount));
    return apiSuccess(answer);
}

static struct json_object * Request(struct json_object * request, struct db * storage) {
    enum action action = getAction(request);

    switch (action) {
        case JSON_API_TYPE_CREATE_TABLE:
            return requestCreateTable(createTableRequest(request), storage);
        case JSON_API_TYPE_DROP_TABLE:
            return requestDropTable(dropTableRequest(request), storage);
        case JSON_API_TYPE_INSERT:
            return requestInsert(insertRequest(request), storage);
        case JSON_API_TYPE_DELETE:
            return requestDelete(deleteRequest(request), storage);
        case JSON_API_TYPE_SELECT:
            return requestSelect(selectRequest(request), storage);
        case JSON_API_TYPE_UPDATE:
            return requestUpdate(updateRequest(request), storage);
        default:
            return NULL;
    }
}

static void handleClient(int socket, struct db * storage) {
    printf("Connected\n");

    while (!closing) {
        char buffer[64 * 1024];

        ssize_t was_read = read(socket, buffer, sizeof(buffer) / sizeof(*buffer));
        if (was_read <= 0) {
            break;
        }

        if (was_read == sizeof(buffer) / sizeof(*buffer)) {
            buffer[sizeof(buffer) / sizeof(*buffer) - 1] = '\0';
        } else {
            buffer[was_read] = '\0';
        }

        struct json_object * request = json_tokener_parse(buffer);
        printf("Request: %s\n", json_object_to_json_string_ext(request, JSON_C_TO_STRING_PRETTY));

        struct json_object * response_object = NULL;

        if (request) {
            response_object = Request(request, storage);
        }

        const char * response = json_object_to_json_string(response_object);
        printf("Response: %s\n", response);

        size_t response_length = strlen(response);
        while (response_length > 0) {
            ssize_t wrote = write(socket, response, response_length);

            if (wrote <= 0) {
                break;
            }
            response_length -= wrote;
            response += wrote;
        }
    }
    close(socket);
    printf("Disconnected\n");
}

int main(int argc, char * argv[]) {
    if (argc < 2) {
        return 0;
    }

    int fd = open(argv[1], O_RDWR);
    struct db * storage;

    if (fd < 0 && errno != ENOENT) {
        perror("Error while opening file");
        return errno;
    }

    if (fd < 0 && errno == ENOENT) {
        fd = open(argv[1], O_CREAT | O_RDWR, 0644);
        storage = initStorage(fd);
    } else {
        storage = openStorage(fd);
    }

    int server_socket;
    server_socket = socket(AF_INET, SOCK_STREAM, 0);

    struct sockaddr_in server_address;
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(9002);
    server_address.sin_addr.s_addr = INADDR_ANY;

    if (bind(server_socket, (struct sockaddr *) &server_address, sizeof(server_address)) != 0) {
        perror("Cannot start server");
        return 0;
    }

    listen(server_socket, 1);

    {
        struct sigaction sa;

        sa.sa_sigaction = closeHandler;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = SA_SIGINFO;

        sigaction(SIGINT, &sa, NULL);
        sigaction(SIGTERM, &sa, NULL);
    }

    while (!closing) {
        int ret = accept(server_socket, NULL, NULL);

        if (ret < 0) {
            break;
        }

        handleClient(ret, storage);
    }

    close(server_socket);
    deleteStorage(storage);
    close(fd);

    printf("Bye!\n");
    return 0;
}
